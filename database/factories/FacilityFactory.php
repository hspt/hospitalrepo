<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class FacilityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'user_id' => random_int(0,50),
            'company_id' => random_int(0,50),
            'description' => $this->faker->name(),
            'image' =>  Str::random(10),
            'fee' =>  0,
            'up_date' => Carbon::now(),
        ];
    }
}
