<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use App\Models\User;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class FacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facil = Facility::all();
       // dd($facil);
        return view('facility.index',['facilities' => $facil]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $user=User::Find(auth::user());
       // dd($user);


       // return view('facility.create',['users'=>$user]);
       return view('facility.create')->with('company',['users'=>$user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $res=new Facility();
        $res->name=$request->input('name');
        $res->description=$request->input('description');
        $res->user_id=$request->input('user_id');
        $res->company_id=$request->input('company_id');
        // $res->image=$request->input('img');
        $res->fee=$request->input('fee');
        $res->up_date=$request->input('up_date');

        $files = [];

       // dd($request->hasfile('images'));

        if($request->hasfile('images'))
        {

            foreach($request->file('images') as $file)
            {

                $name = time().rand(1,100).'.'.$file->extension();

                $file->move(public_path('product-images'), $name);

                $files[] = $name;

            }

        }

        foreach($files as $f)
        {

            $res->image = $f;
            //$res->product_id = $lastId;
            $res->save();
        }




       // $res->save();
        // dd($res);
        $request->session()->flash('msg','Data saved successfully');
         return redirect('admin/facility');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function show(Facility $facility)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {

        $request=Facility::Find($id);
        // $edi->name=$request->input('name');
        // $edi->save();

    // dd($id);
       return view('facility.edit')->with('facility',Facility::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Facility $facility)
    {

        $res=Facility::find($facility->id);
        // dd($facility->id);
        $res->name=$request->input('name');
        $res->save();
        $request->session()->flash('msg','Data saved successfully');
         return redirect('admin/facility');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Facility  $facility
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {

        Facility::destroy($id);
         $request->session()->flash('msg','Record deleted successfully');
        return redirect('admin/facility');
    }

}
