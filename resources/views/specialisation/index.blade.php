@extends('layouts.app');
@section('content')

    <div class="container mt-2">
          <h3>Specialisations</h3>

          <ul class="nav nav-tabs">
          <li class="nav-item">  <a class="nav-link active" href="#">Specialisations </a> </li>
          <li class="nav-item">  <a class="nav-link" href="{{ url('/specialisation/create') }}">New Specialisation</a> </li>
          {{session('msg')}}
          <br/>

          </ul>
          <div>

            <table class="table">
                <thead class="table-dark">
                    <tr>
                    <th>Id</th>
                    <th>Specialisation</th>
                    <th colspan="2">
                    Actions
                    </th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($spec as $d)
                    <tr>
                    <td>{{$d->id}}</td>
                    <td>{{$d->name}}</td>
                    <td colspan="2">
                    <a href="specialisation/edit/{{$d->id}}"> <i class="bi bi-pencil-square"></i></a>
                    <a href="specialisation/delete/{{$d->id}}"> <i class="bi bi-trash"></i></a>
                   <a href="specialisation/delete/{{$d->id}}"> <i class="bi bi-eye-fill"></i></a>

                    </td>



                 </tr>
                @endforeach
                </tbody>
              </table>
      </div>

    </div>




@endsection

