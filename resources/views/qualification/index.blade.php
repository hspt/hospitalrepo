@extends('layouts.navbar')
{{-- @extends('layouts.app') --}}
@section('link')
<link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
@section('content')
<body>
     <div class="container mt-2">
          <h3>Qualifications</h3>
     </div>

        <ul class="nav nav-tabs">
          <li class="nav-item">  <a class="nav-link active" href="{{ url('/qualification') }}">Qualifications </a> </li>
          <li class="nav-item">  <a class="nav-link" href="qualification/create">New Qualification</a> </li>
          {{session('msg')}}
          <br/>

        </ul>

          <div class="tab-content">
            <table class="table">
                <thead class="table-dark">
                    <tr>
                    <th>Id</th>
                    <th>Qualification</th>
                    <th colspan="2">
                    Actions
                    </th>
                </tr>
                </thead>
                @foreach ($qualifications as $d)
                <tr>
                    <td>{{$d->id}}</td>
                    <td>{{$d->name}}</td>
                    <td>
                    <a href="qualification/{{$d->id}}/edit"> <i class="bi bi-pencil-square"></i></a>
                    <a href="qualification/delete/{{$d->id}}"> <i class="bi bi-trash"></i></a>
                   <a href="qualification/view/{{$d->id}}"> <i class="bi bi-eye-fill"></i></a>
                    </td>
                </tr>


            @endforeach
        </table>
    </div>

  </div>



</body>
@endsection
