@extends('layouts.navbar')
@section('link')
<link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
@section('content')

    <div class="container mt-2">
          <h3>Departments</h3>

          <ul class="nav nav-tabs">
          <li class="nav-item">  <a class="nav-link active" href="{{ url('/department') }}">Departments </a> </li>
          <li class="nav-item">  <a class="nav-link"href="{{ url('/department/create') }}">New Department</a> </li>
          {{session('msg')}}
          <br/>

          </ul>
          <div>

            <table class="table">
                <thead class="table-dark">
                    <tr>
                    <th>Id</th>
                    <th>Department</th>
                    <th colspan="2">
                    Actions
                    </th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($departments as $d)
                    <tr>
                    <td>{{$d->id}}</td>
                    <td>{{$d->name}}</td>
                    <td colspan="2">
                    <a href="department/edit/{{$d->id}}"> <i class="bi bi-pencil-square"></i></a>
                    <a href="department/delete/{{$d->id}}"> <i class="bi bi-trash"></i></a>
                   <a href="department/delete/{{$d->id}}"> <i class="bi bi-eye-fill"></i></a>

                    </td>



                 </tr>
                @endforeach
                </tbody>
              </table>
      </div>

    </div>

@endsection

