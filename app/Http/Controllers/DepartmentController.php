<?php

namespace App\Http\Controllers;

use App\Models\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
       //dd($departments) ;
       //return view('department.index',compact('departments'));
       return view('department.index',['departments' => $departments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->input();

        $res=new Department();
        $res->name=$request->input('name');
        $res->save();
        $request->session()->flash('msg','Data saved successfully');
         return redirect('/department');
        // return view('department.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department,$id)
    {

        $department = Department::find($id);

        return view('department.edit')->with('department',Department::find($id));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $res=Department::find($request->id);
        $res->name=$request->input('name');
        $res->save();
        $request->session()->flash('msg','Data saved successfully');
         return redirect('/department');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Department $department,$id)
    {
        Department::destroy($id);
         $request->session()->flash('msg','Record deleted successfully');
        return redirect('/department');
    }
}
