<?php

namespace App\Http\Controllers;

use App\Models\Qualification;
use Illuminate\Http\Request;

class QualificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $qualifications=Qualification::all();
        // dd($qualifications);
        return view('qualification.index',compact('qualifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('qualification.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->qualification);
        // return $request->input();
        $res=new Qualification();
        $res->name=$request->input('name');
        $res->save();
        session()->flash('message', 'Successfully Saved');
        return redirect('qualification');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return view('qualification.edit');
        $product = Qualification::find($id);

        return view(
            'qualification.edit',
            [
                'qualificationArr' => $product
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
<<<<<<< HEAD
    public function edit(Qualification $qualification,$id)
    {
        // dd($qualification);
        $spec = Qualification::find($id);

        return view('qualification.edit')->with('qualificationArr',Qualification::find($id));
=======
    public function edit($id)
    {
        // dd($qualification);
         return view('qualification.edit')->with('qualification',Qualification::find($id));
>>>>>>> fa493aa8de93aa475f52d8283edf50c6f12b3d18
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Qualification $qualification)
    {
        $res=Qualification::find($qualification->id);
        $res->name=$request->input('name');
        // dd($res);
        $res->save();
        session()->flash('message', 'Successfully Updated');
        return redirect('qualification');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Qualification  $qualification
     * @return \Illuminate\Http\Response
     */

     public function destroy(Request $request,$id)
    {
        Qualification::destroy($id);
        $request->session()->flash('msg','Record deleted successfully');
        return redirect('qualification');
    }
}
