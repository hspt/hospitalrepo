@extends('layouts.app');
@section('content')
<body>
    <div class="container mt-2">
          <h3>Facilities</h3>

          <ul class="nav nav-tabs">
          <li class="nav-item">  <a class="nav-link" href="#">Edit Facility </a> </li>
          <li class="nav-item">  <a class="nav-link active" href="create">New Facility</a> </li>
          {{session('msg')}}
          <br/>

          </ul>
          <div class="tab-content">
              <div class="container mt-2">
              <form method="POST" action="{{ route('facility.update',$facility->id) }}">
                  @csrf
                  {{ method_field('PUT') }}
                      <table>

                          <tr>
                              <td>Facility Name :</td>
                              <td> <input type="text" id="name" name="name" value="{{$facility->name}}"></td>
                          </tr>
                          <tr>
                              <td> <input type="submit" id="btnupd" name="btnupd" value="Save"></td>
                          </tr>
                      </table>
                  </form>
              </div>
      </div>

    </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>


</body>
@endsection
