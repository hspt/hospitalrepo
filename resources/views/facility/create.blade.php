@extends('layouts.app');
@section('content')
<body>
    <div class="container mt-2">
          <h3>Facilities</h3>

          <ul class="nav nav-tabs">
          <li class="nav-item">  <a class="nav-link" href="#">Facilities </a> </li>
          <li class="nav-item">  <a class="nav-link active" href="create">New Facility</a> </li>
          {{session('msg')}}
          <br/>

          </ul>
          <div class="tab-content">
              <div class="container mt-2">
              <form method="post" action="{{ route('facility.store') }}" enctype="multipart/form-data">
                  @csrf
                      <table>

                          <tr>
                              <td>Facility Name :</td>
                              <td> <input type="text" id="name" name="name"></td>
                          </tr>
                          <tr>
                            <td>Description :</td>
                           <td> <textarea name="description" id="description"></textarea></td>
                          </tr>

                          {{-- <tr>
                            <td>User ID:</td>
                            <td> <input type="number" id="user_id" name="user_id"></td>
                          </tr> --}}

                          <tr>
                            <td>User :</td>
                            <td>

                            <select name="user_id" class="form-control">
                                @foreach ($users as $c)
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                @endforeach

                            </select>
                            </td>
                          </tr>
                          <tr>
                            <td>Company :</td>
                            <td>
                                {{$users->company->name}}
                            {{-- <select name="company_id" class="form-control">
                                @foreach ($companies as $c)
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                @endforeach

                            </select> --}}
                        </td>
                          </tr>


                          {{-- <tr>
                            <td>Company ID:</td>
                            <td> <input type="number" id="company_id" name="company_id"></td>
                          </tr> --}}

                          <tr>
                            <td>Fee:</td>
                            <td> <input type="number" id="fee" name="fee"></td>
                          </tr>

                          <tr>
                            <td>Up Date:</td>
                            <td> <input type="date" id="up_date" name="up_date"></td>
                          </tr>

                          <tr>
                            <td>Image:</td>
                            {{-- <td> <input type="text" id="img" name="img"></td> --}}

                            <td><input type="file" multiple name="images[]" id="title"> </td>

                          </tr>

                          <tr>
                              <td> <input type="submit" id="btnsave" name="btnsave"></td>
                          </tr>
                      </table>
                  </form>
              </div>
      </div>

    </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>


</body>
@endsection
