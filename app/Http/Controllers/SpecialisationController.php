<?php

namespace App\Http\Controllers;

use App\Models\Specialisation;
use Illuminate\Http\Request;

class SpecialisationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $specs=Specialisation::all();
      // dd($specs);
       return view('specialisation.index',['spec'=>$specs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('specialisation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $res=new Specialisation();
        $res->name=$request->input('name');
        $res->save();
        $request->session()->flash('msg','Data saved successfully');
         return redirect('/specialisation');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Specialisation  $specialisation
     * @return \Illuminate\Http\Response
     */
    public function show(Specialisation $specialisation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Specialisation  $specialisation
     * @return \Illuminate\Http\Response
     */
    public function edit(Specialisation $specialisation,$id)
    {
        $spec = Specialisation::find($id);

        return view('specialisation.edit')->with('spec',Specialisation::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Specialisation  $specialisation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Specialisation $specialisation)
    {
        $res=Specialisation::find($request->id);
        $res->name=$request->input('name');
        $res->save();
        $request->session()->flash('msg','Data saved successfully');
         return redirect('/specialisation');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Specialisation  $specialisation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Specialisation $specialisation,$id)
    {
        Specialisation::destroy($id);
         $request->session()->flash('msg','Record deleted successfully');
        return redirect('/specialisation');
    }
}
