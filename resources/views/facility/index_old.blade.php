@extends('layouts.app');
@section('content')

    <div class="container mt-2">
          <h3>Departments</h3>

          <ul class="nav nav-tabs">
          <li class="nav-item">  <a class="nav-link active" href="#">Facilities </a> </li>
          <li class="nav-item">  <a class="nav-link" href="facility/create">New Facility</a> </li>
          {{session('msg')}}
          <br/>

          </ul>
          <div>

            <table class="table">
                <thead class="table-dark">
                    <tr>
                    <th>Id</th>
                    <th>Facility</th>
                    <th colspan="2">
                    Actions
                    </th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($facilities as $d)
                    <tr>
                    <td>{{$d->id}}</td>
                    <td>{{$d->name}}</td>
                    <td colspan="2">
                    <a href="facility/{{$d->id}}/edit"> <i class="bi bi-pencil-square"></i></a>
                    <a href="facility/{{$d->id}}"> <i class="bi bi-trash"></i></a>
                   <a href="facility/delete/{{$d->id}}"> <i class="bi bi-eye-fill"></i></a>

                    </td>



                 </tr>
                @endforeach
                </tbody>
              </table>
      </div>

    </div>




@endsection

