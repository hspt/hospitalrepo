@extends('layouts.navbar')
@section('link')
<link href="../../assets/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="../../assets/dist/js/bootstrap.bundle.min.js"></script>
@section('content')
<body>
    <div class="container mt-2">
          <h3>Qualifications</h3>

          <ul class="nav nav-tabs">
          <li class="nav-item">  <a class="nav-link" href="{{ url('/qualification') }}">Qualifications </a> </li>
          <li class="nav-item">  <a class="nav-link active" href="{{ url('/qualification/create') }}">New Qualification</a> </li>
          {{session('msg')}}
          <br/>

          </ul>
          <div class="tab-content">
              <div class="container mt-2">
                <form method="post" action="../qualification">
                    @csrf
                    <div class="form-group">
                    <label>Qualification</label>
                    <input type="text" name="name" value="">
                    </div>
                    <div class="form-group">
                        <button type="submit" >Submit</button>
                    </div>
                </form>
            </div>
        </div>

      </div>
    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> --}}


  </body>
@endsection
