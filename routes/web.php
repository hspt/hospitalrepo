<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','App\Http\Controllers\LoginController@index');
Route::post('authenticate','App\Http\Controllers\LoginController@authenticate');
Route::get('/logout','App\Http\Controllers\LoginController@logout');

Route::get('dashboard', function () {
    return view('dashboard');
});


// Route::get('qualification','App\Http\Controllers\QualificationController');
// Route::get('qualification_create','App\Http\Controllers\QualificationController@create');
// Route::post('qualification_submit','App\Http\Controllers\QualificationController@store');
// Route::get('qualification_edit/{id}','App\Http\Controllers\QualificationController@edit');
// Route::post('qualification_update/{id}','App\Http\Controllers\QualificationController@update');
// Route::get('qualification_delete/{id}','App\Http\Controllers\QualificationController@destroy');
// Route::prefix('admin')->middleware('admin')->group(function () {
    // Route::get('qualification', 'App\Http\Controllers\QualificationController@index');
    // Route::get('qualification_create','App\Http\Controllers\QualificationController@create');
    // Route::post('qualification_submit','App\Http\Controllers\QualificationController@store');
    // Route::get('qualification_edit/{id}','App\Http\Controllers\QualificationController@edit');
    // Route::post('qualification_update/{id}','App\Http\Controllers\QualificationController@update');
    // Route::get('qualification_delete/{id}','App\Http\Controllers\QualificationController@destroy');
    Route::resource('qualification', 'App\Http\Controllers\QualificationController');
// });

Route::get('department','App\Http\Controllers\DepartmentController@index');
Route::get('department/create','App\Http\Controllers\DepartmentController@create');
Route::post('department/submit','App\Http\Controllers\DepartmentController@store');
Route::get('department/edit/{id}','App\Http\Controllers\DepartmentController@edit');
Route::post('department/update/{id}','App\Http\Controllers\DepartmentController@update');
Route::get('department/delete/{id}','App\Http\Controllers\DepartmentController@destroy');

Route::get('specialisation','App\Http\Controllers\SpecialisationController@index');

Route::get('specialisation/create','App\Http\Controllers\SpecialisationController@create');
Route::post('specialisation/submit','App\Http\Controllers\SpecialisationController@store');
Route::get('specialisation/edit/{id}','App\Http\Controllers\SpecialisationController@edit');
Route::post('specialisation/update/{id}','App\Http\Controllers\SpecialisationController@update');
Route::get('specialisation/delete/{id}','App\Http\Controllers\SpecialisationController@destroy');

// Route::resource('facility', 'App\Http\Controllers\FacilityController');

Route::prefix('admin')->middleware('admin')->group(function () {
    Route::resource('facility', 'App\Http\Controllers\FacilityController');
});

