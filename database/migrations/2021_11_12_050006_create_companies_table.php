<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('area');
            $table->string('address1');
            $table->string('address2');
            $table->string('gmap');
            $table->string('fid');
            $table->string('departments');
            $table->string('photo');
            $table->string('aboutclinic');
            $table->string('contact');
            $table->string('telephone');
            $table->string('fax');
            $table->string('emergency_number');
            $table->string('email');
            $table->string('email_appointment');
            $table->string('url');
            $table->decimal('latitude');
            $table->decimal('longitude');
            $table->string('fb');
            $table->string('google');
            $table->string('twitter');
            $table->string('image_show');
            $table->char('drcmshis');
            $table->char('displayhospitalnumberin');
            $table->string('youtubelink');
            $table->string('payment_terms');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
